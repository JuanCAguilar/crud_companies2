from django.apps import AppConfig


class DatosempresaAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'datosempresa_app'
