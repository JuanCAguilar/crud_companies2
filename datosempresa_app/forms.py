from django import forms    
from .models import Company

class CompanyForm(forms.Form):
    name_company=forms.CharField(max_length = 20)
    description=forms.CharField(required=False,widget=forms.Textarea(attrs={'row':5,'cols':30}))
    address=forms.CharField(max_length=20, required=True)
    nit=forms.IntegerField(min_value=0)
    
    class Meta:
        model = Company


