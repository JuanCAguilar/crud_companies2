from django.db import models

# Create your models here.
class Company(models.Model):
    name_company = models.CharField(max_length=100)
    address = models.TextField(blank=True)
    nit = models.IntegerField()
    description=models.TextField(blank=True)
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name='Company'
        verbose_name_plural='Companys'

    def __str__(self):
        return self.name_company            