
from django.contrib import admin
from django.urls import path
from .views import empresaListDatos,createCompany, search,delete_company,update_company,update
from django.contrib.auth import views as auth_views

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    
    path('', empresaListDatos,name="empresaListDatos" ),
    path('modify/', createCompany,name="createCompany" ),
    path('search/', search,name="search" ),
    path('delete/<int:id_company>/', delete_company,name="delete_company" ),
    path('update_company/<int:id_company>/', update_company,name="update_company" ),
    path('update/', update,name="update"),
    
    
    # path('login/', auth_views.LoginView.as_view(template_name='/login.html'),name='login'),
    # path('logout/', auth_views.LogoutView.as_view(template_name='/login.html'),name='loguot'),
    
] 
