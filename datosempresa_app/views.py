from django.http import HttpResponse
from django.shortcuts import render,redirect
# from django.contrib.auth.mixins import  LoginRequiredMixin
from django.views import generic
from .forms import CompanyForm
from .models import Company


def empresaListDatos(request):
    company=Company.objects.all()
    return render(request,"empresa_List.html",{'company':company})

def createCompany(request):
    form=CompanyForm()
    return render(request,"modify_company.html",{'form':form})

def search(request):
    
    company=Company(name_company=request.POST['name_company'],
            description=request.POST['description'],
            address=request.POST['address'],
            nit=request.POST['nit'],
            )
    company.save()
    
    # return HttpResponse(element)
    return redirect('/')  
    
def delete_company(request,id_company):
    deleteCompany=Company.objects.get(id=id_company)
    deleteCompany.delete() 
    return redirect('/')  
    
    
def update_company(request,id_company):
    updateCompany=Company.objects.get(id=id_company)
    print(updateCompany.id)
    form=CompanyForm()
    return render(request,"update_company.html",{'form':form,'updateCompany':updateCompany}) 

def update(request):
    id= int(request.POST['id'])
    print(id)
    updateCompany=Company.objects.get(id=id)
    updateCompany.name_company=request.POST['name_company']
    updateCompany.description=request.POST['description'],
    updateCompany.address=request.POST['address'],
    updateCompany.int(nit=request.POST['nit']),
    updateCompany.save()
    
    # return HttpResponse(element)
    return redirect('/')  

